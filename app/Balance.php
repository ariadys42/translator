<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
  protected $fillable = [
    'id', 'amount'
  ];

  public function user_balance() {
    return $this->hasMany('App\User');
  }

}
