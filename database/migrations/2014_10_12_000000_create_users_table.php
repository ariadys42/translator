<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id','50')->primary();
            $table->string('username');
            $table->string('fullname');
            $table->string('email')->unique();
            $table->timestamp('email_verified')->nullable();
            $table->string('password');
            $table->string('level');
            $table->string('photo');
            $table->string('birthdate');
            $table->string('gender');
            $table->string('verified')->default(0);
            $table->string('city');
            $table->string('phone');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
